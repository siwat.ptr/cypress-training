describe('', () => {
	before(() => {
		cy.log('Before')
	})

	beforeEach(() => {
		cy.log('Before each')
	})

	after(() => {
		cy.log('After')
	})

	afterEach(() => {
		cy.log('After each')
	})

	it('Plus number', () => {
		let result = 1 + 2
		expect(result).equal(3)
	})

	it('Subtract number', () => {
		let result = 9 - 2
		cy.wrap(result).should('equal', 7)
	})

	it('Divide number', () => {
		let result = 9 / 3
		assert.equal(result, 3)
	})
})
