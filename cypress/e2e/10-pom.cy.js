const IndexPage = require('../page-objects/indexPage')
const LoginPage = require('../page-objects/loginPage')

describe('Login', () => {
	beforeEach(() => {
		IndexPage.navigateToUrl('https://katalon-demo-cura.herokuapp.com/')
	})

	it('Can display alert message when input invalid', () => {
		IndexPage.clickbtnMakeAppointment()
		LoginPage.inputLogin('John Doe ja', 'ThisIsNotAPassword')
		LoginPage.canDisplayAlert(
			'Login failed! Please ensure the username and password are valid.'
		)
	})
})
