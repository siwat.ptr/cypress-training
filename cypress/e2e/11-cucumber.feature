Feature: Login Feature

        As a valid user
        I want to login to application

        Scenario: Login with valid user
                Given I open page
                And I click make appointment button to login page
                When I fill username "John Doe"
                And I fill password "ThisIsNotAPassword"
                And I click login button
                Then I can see appointment page