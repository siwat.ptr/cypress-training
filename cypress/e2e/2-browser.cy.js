describe('Browser command', () => {
	it('Browser', () => {
		cy.visit('https://katalon-demo-cura.herokuapp.com/')

		cy.title().should('eq', 'CURA Healthcare Service')

		cy.url().should('eq', 'https://katalon-demo-cura.herokuapp.com/')
		cy.url().should('contain', 'https')

		cy.viewport('iphone-7')
		cy.viewport(1920, 1080)
	})
})
