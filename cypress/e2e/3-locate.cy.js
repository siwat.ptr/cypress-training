describe('Locate Element', () => {
	it('Locate Element', () => {
		cy.visit('https://katalon-demo-cura.herokuapp.com/')

		// Get element by id
		cy.get('#btn-make-appointment').should('contain.text', 'Make Appointment')

		// Get element by text
		cy.contains('We Care About Your Health').should('exist')

		// Get element by HTML tag [indexing]
		cy.get('a').eq(0).should('have.class', 'btn btn-dark btn-lg toggle')

		// Get element by id then find element inside child
		cy.get('#top')
			.find('div')
			.children()
			.eq(0)
			.should('contain.text', 'CURA Healthcare Service')
	})
})
