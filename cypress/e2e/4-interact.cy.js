describe('Interact', () => {
	it('Interact', () => {
		cy.visit('https://katalon-demo-cura.herokuapp.com/')

		// login
		cy.get('#btn-make-appointment').click()
		cy.get('#txt-username').type('John Doe')
		cy.get('#txt-password').type('ThisIsNotAPassword')
		cy.get('#btn-login').click()

		// select facility
		cy.get('#combo_facility')
			.select('Hongkong CURA Healthcare Center')
			.should('have.value', 'Hongkong CURA Healthcare Center')
		// select check box
		cy.get('#chk_hospotal_readmission').check()
		// click radio
		cy.get('#radio_program_medicaid').click()
	})
})
