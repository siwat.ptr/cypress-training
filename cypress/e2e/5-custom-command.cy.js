describe('Custom Command', () => {
	it('Custom Command', () => {
		cy.visit('/')
		cy.title().should('be.equal', 'CURA Healthcare Service')
		cy.get('#btn-make-appointment').click()
		cy.login('John Doe', 'ThisIsNotAPassword')
	})
})
