describe('Invoke Command', () => {
	it('Get web element properties', () => {
		cy.visit('https://katalon-demo-cura.herokuapp.com/')
		cy.get('#btn-make-appointment')
			.invoke('attr', 'href')
			.should('contain', 'profile.php#login')
	})

	it('Set web element properties', () => {
		cy.visit('https://testautomationpractice.blogspot.com/')
		cy.get('#slider > span').invoke('attr', 'style', 'left: 50%')
	})

	it('Set value', () => {
		cy.visit('https://testautomationpractice.blogspot.com/')
		cy.get('#name').invoke('val', 'John')
	})
})

describe('Wrap Command', () => {
	it('Wrap web elements to use cypress commands', () => {
		cy.visit('https://testautomationpractice.blogspot.com/')
		cy.get('#slider > span').then(elm => {
			cy.wrap(elm).should(
				'have.class',
				'ui-slider-handle ui-state-default ui-corner-all'
			)
		})
	})
})
