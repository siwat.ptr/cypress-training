describe('Alias command', () => {
	beforeEach(() => {
		cy.fixture('data.json').as('data')
	})

	it('Reuse web element', () => {
		cy.visit('/')

		cy.get('#btn-make-appointment').as('linkMakeAppointment')
		cy.get('@linkMakeAppointment').should('exist')
		cy.get('@linkMakeAppointment').click()
	})

	it('Method1 Reuse value in then', () => {
		cy.visit('/')

		cy.get('#btn-make-appointment')
			.then(elm => {
				let text = elm.text()
				return text
			})
			.as('btnText')

		cy.get('@btnText').should('contain', 'Make Appointment')
	})

	it('Method2 Reuse value in then', () => {
		cy.visit('/')

		cy.get('#btn-make-appointment').then(elm => {
			let text = elm.text()
			cy.wrap(text).as('btnText')
		})

		cy.get('@btnText').should('contain', 'Make Appointment')
	})

	it('Alias for getting test data', function () {
		expect(this.data.phone).equal('123456789')
	})

	it('Then for getting test data', () => {
		cy.fixture('data.json').then(data => {
			expect(data.phone).equal('123456789')
			cy.wrap(data.phone).as('myphone')
		})
	})
})
