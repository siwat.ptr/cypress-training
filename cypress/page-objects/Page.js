class Page {
	static navigateToUrl(url) {
		cy.visit(url)
	}

	static canNavigateToUrl(url) {
		cy.url().should('eq', url)
	}
}

module.exports = Page
